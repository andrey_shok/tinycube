package com.cubes;

import com.badlogic.gdx.math.Intersector;
import java.io.IOException;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;

/*	This class load and draw levels
 * Not yeat make
 */
public class GameScreen implements Screen {
	// Global parametres-----------------------------------------------
	final Drop game;

	Texture dropImage;
	Texture bucketImage;
	Texture BlockImage;
	Texture BlockImage2;
	Texture BlockImage3;
	Texture background;
	Texture FrontText;
	Texture Sprin;
	Texture Spike;
	Texture portalText;
	Texture keyText;
	TextureRegion cubeReg;
	Persons Cube;
	Sound dropSound;
	Music rainMusic;

	OrthographicCamera camera;

	Rectangle backgroundRect;
	Rectangle backgroundRect2;
	Rectangle loadRect;
	int x, y, x_s = 0, y_s = 0, x_m, y_m;
	int widthBlocks = 50;
	int step = 0, curStep = 0, curGrav = 0, up;
	int currentX = 0, currentY = 0, numMess = 0, curUp = 0;
	long sizeBlock;
	float width;
	float heigth;
	Array<Rectangle> raindrops;

	Block[][] cur_level;

	char[] str;
	float delta;
	long lastDropTime;
	int dropsGathered;
	volatile boolean wag = false, jump = false, gravitation = false,
			god = true, lastGod = true, showMess = false, upWall = false,
			moveWall = true, keyInBlock = false;
	Texture games;
	Persons persons;
	String levelStr, currentStr;
	Texture loadText;
	boolean load;
	int numLoad = 0;
	int level, world;

	// -----------------------------------------------------------------------------
	/*
	 * Constructor this class, he load's levels from files
	 */
	public GameScreen(final Drop gam, int world, int level)
			throws NumberFormatException, IOException {
		this.world = world;
		this.level = level;
		this.game = gam;
		loadText = new Texture("Game/loading.jpeg");
		loadRect = new Rectangle(
				(-1)
						* Math.abs((loadText.getWidth() / 2 - Gdx.graphics
								.getWidth()) / 2), (-1)
						* Math.abs((loadText.getHeight() / 2 - Gdx.graphics
								.getHeight()) / 2), loadText.getWidth() / 2,
				loadText.getHeight() / 2);

		// Connect camera-------------------------------------------
		camera = new OrthographicCamera();
		camera.setToOrtho(false, Gdx.graphics.getWidth(),
				Gdx.graphics.getHeight());
		// ----------------------------------
		widthBlocks = (int) sizeBlock;
		sizeBlock = 50;
	}

	public void getNextString() {
		String time;
		int i, j;
		time = "";
		i = 0;
		if (levelStr.length() != 0) {
			while (levelStr.charAt(i) != '\n') {
				time += levelStr.charAt(i);
				i++;
			}
			i++;
		}
		currentStr = time;
		time = "";
		for (j = i; j < levelStr.length(); j++) {
			time += levelStr.charAt(j);
		}
		levelStr = time;
	}

	public int getNextElement() {
		int num, i, j;
		String time;
		i = 0;
		time = "";
		num = 0;
		if (currentStr.length() != 0) {
			while (currentStr.charAt(i) != ' ') {
				time += currentStr.charAt(i);
				i++;
			}
			i++;
			try {
				num = Integer.parseInt(time);
			} catch (NumberFormatException ex) {
				num = (int) time.charAt(0);
				keyInBlock = true;
			}
			// System.out.print(num);
			time = "";
		}
		for (j = i; j < currentStr.length(); j++) {
			time += currentStr.charAt(j);
		}
		currentStr = time;
		return num;
	}

	public String getNextElementString() {
		int i;
		String time;
		i = 0;
		time = "";
		if (currentStr.length() != 0) {
			while (currentStr.charAt(i) != ' ') {
				time += currentStr.charAt(i);
				i++;
			}
			i++;
		}
		return time;
	}

	private void loading() {
		int num_str, num_int, numMess, mess, numElement, portalNum, portal, x_t, y_t, x_t2, y_t2, keyNum, key, type; // Iterater
																														// for
		god = false; // 'for')
		System.out
				.println("Hello it's DEBUG mode in release he will not work.");
		if (god) {
			System.out.println("God Mode:On. Have Fun!");
		} else {
			System.out.println("God Mode:Off. So sad:(");
		}
		// Load texture---------------------------------------------------------
		BlockImage2 = new Texture(Gdx.files.internal("Game/Block1.png"));
		BlockImage3 = new Texture(Gdx.files.internal("Game/Block2.png"));
		BlockImage = new Texture(Gdx.files.internal("Game/Cube.png"));
		Sprin = new Texture(Gdx.files.internal("Game/Sprint_T.png"));
		background = new Texture(Gdx.files.internal("Game/time_background.png"));
		Spike = new Texture(Gdx.files.internal("Game/Spike.png"));
		FrontText = new Texture(Gdx.files.internal("Game/Font.png"));
		portalText = new Texture(Gdx.files.internal("Game/Portal.png"));
		keyText = new Texture(Gdx.files.internal("Game/Key.png"));
		// -------------------------------------------------------------------------
		// Init
		// rectangle-----------------------------------------------------------
		backgroundRect = new Rectangle(0, 0, Gdx.graphics.getWidth(),
				Gdx.graphics.getHeight());
		backgroundRect2 = new Rectangle(Gdx.graphics.getWidth(), 0,
				Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		// ----------------------------------------------------------------------
		// Start load levels --------------------------------------------------
		FileHandle file_level = Gdx.files.internal("Level/World" + world
				+ "/Level" + level + "/Level.lvl");
		levelStr = file_level.readString();
		// End load levels -------------------------------------------------
		// Read parametrs map ------------------------------------------
		getNextString();
		y = Integer.parseInt(currentStr);
		getNextString();
		x = Integer.parseInt(currentStr);

		numMess = 0;

		cur_level = new Block[x + 1][y + 1];
		for (num_str = 0; num_str < y; num_str++) {
			getNextString();
			for (num_int = 0; num_int < x; num_int++) {
				type = getNextElement();
				if (keyInBlock) {
					keyInBlock = false;
					cur_level[num_int][num_str] = new Block(2, new Rectangle(
							num_int * sizeBlock,
							(Gdx.graphics.getHeight() - num_str * sizeBlock),
							sizeBlock, sizeBlock), BlockImage, 0);
					cur_level[num_int][num_str].addKey((char) type);
				} else {
					cur_level[num_int][num_str] = new Block(type,
							new Rectangle(num_int * sizeBlock, (Gdx.graphics
									.getHeight() - num_str * sizeBlock),
									sizeBlock, sizeBlock), BlockImage, 0);
					if (cur_level[num_int][num_str].getType() > 50) {
						cur_level[num_int][num_str]
								.setVelo(cur_level[num_int][num_str].getType());
						cur_level[num_int][num_str].setType(4);
						cur_level[num_int][num_str].setTexture(Sprin);
					} else {
						if (cur_level[num_int][num_str].getType() > 20) {
							numMess += 1;
						}
					}
					if (cur_level[num_int][num_str].getType() == 5) {
						cur_level[num_int][num_str].setTexture(Spike);
					}
					if (cur_level[num_int][num_str].getType() == 6) {
						cur_level[num_int][num_str].setTexture(BlockImage);
					}
				}

			}
		}
		// System.out.println(numMess);
		// --------------------------------------------------
		// Read Frendly Person index , Texture
		// -------------------------------------------------------
		for (mess = 0; mess < numMess; mess++) {
			getNextString();
			numElement = getNextElement();
			// System.out.println(numElement);
			for (num_str = 0; num_str < y; num_str++) {
				for (num_int = 0; num_int < x; num_int++) {
					// System.out.println(cur_level[num_str][num_int].getType());
					if (cur_level[num_int][num_str].getType() == numElement) {
						cur_level[num_int][num_str].setType(10);
						cur_level[num_int][num_str].setMessText(new Texture(
								"Level/World" + world + "/Level" + level + "/"
										+ getNextElementString()));
						cur_level[num_int][num_str].setTexture(new Texture(
								Gdx.files.internal("Game/Cube_GG.png")));
					}
				}
			}
		}
		// --------------------------------------------------------------------------
		// Read Portals x1 y1 x2 y2
		// Texture;-------------------------------------------
		getNextString();
		portalNum = getNextElement();
		System.out.println(portalNum);
		for (portal = 0; portal < portalNum; portal++) {
			getNextString();
			x_t = getNextElement();
			y_t = getNextElement();
			x_t2 = getNextElement();
			y_t2 = getNextElement();
			cur_level[x_t][y_t].setPortal(true);

			cur_level[x_t][y_t].setPortalRect(cur_level[x_t2][y_t2]
					.getRectangle());
			if (getNextElement() == 1) {
				cur_level[x_t][y_t].setType(-1);
				cur_level[x_t][y_t].setTexture(portalText);
			}
		}
		// ---------------------------------------------------------------------------
		// Install key x y
		// type------------------------------------------------------
		getNextString();
		keyNum = getNextElement();
		for (key = 0; key < keyNum; key++) {
			getNextString();
			x_t = getNextElement();
			y_t = getNextElement();
			cur_level[x_t][y_t].setItemKey((char) getNextElement());
			cur_level[x_t][y_t].setTexture(keyText);
			cur_level[x_t][y_t].setType(-1);
		}
		// ----------------------------------------------------------------------------
		// Install
		// camera---------------------------------------------------------
		camera = new OrthographicCamera();
		camera.setToOrtho(false, Gdx.graphics.getWidth(),
				Gdx.graphics.getHeight());
		// -------------------------------------------------------------------------
		// Set input
		// listner---------------------------------------------------------
		Gdx.input.setInputProcessor(new Input());
		// End-----------------------------------------------------------------------
		// Fix bag(1)-------------------------
		/*
		 * width=(float) (800.0/Gdx.graphics.getWidth()); heigth=(float)
		 * (600.0/Gdx.graphics.getHeight());
		 */
		width = 1;
		heigth = 1;
		// --------------------------------------------------------------
		parseStart();
		Cube = new Persons(new Rectangle(currentX * sizeBlock, currentY
				* sizeBlock, sizeBlock, sizeBlock), 0);
		cubeReg = new TextureRegion();
		cubeReg.setRegion(new Texture("Game/Cube_GG.png"));
		gravity(Cube);

		load = true;
		// ---------------------------------------------------------
	}

	public void render(float delta) {
		int x_c, y_c;
		Rectangle timeRect;

		Gdx.gl.glClearColor(0, 0, 0.2f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		if (numLoad < 90) {
			if ((load == false) && (numLoad > 0)) {
				loading();
			}
			game.batch.setProjectionMatrix(camera.combined);
			camera.update();

			game.batch.begin();
			game.batch.draw(loadText, loadRect.x, loadRect.y, loadRect.width,
					loadRect.height);
			game.batch.end();

			numLoad++;
		} else {
			update(Cube);
			this.delta = delta;
			camera.update();
			game.batch.setProjectionMatrix(camera.combined);
			game.batch.begin();
			game.batch.draw(background, backgroundRect.x, backgroundRect.y,
					backgroundRect.width, backgroundRect.height);
			game.batch.draw(background, backgroundRect2.x, backgroundRect2.y,
					backgroundRect2.width, backgroundRect2.height);
			for (y_c = 0; y_c < y; y_c++) {
				for (x_c = 0; x_c < x; x_c++) {
					timeRect = cur_level[x_c][y_c].getRectangle();
					if ((cur_level[x_c][y_c].getType() != 0)
							&& (cur_level[x_c][y_c].getType() != 3)
							&& (cur_level[x_c][y_c].getType() != 6)
							&& (timeRect.x <= Gdx.graphics.getWidth())
							&& (timeRect.x >= (-1) * sizeBlock)
							&& (timeRect.y <= Gdx.graphics.getHeight())
							&& (timeRect.y >= (-1) * sizeBlock)) {
						game.batch.draw(cur_level[x_c][y_c].getText(),
								timeRect.x, timeRect.y, timeRect.width,
								timeRect.height);
					}
				}
			}

			game.batch.draw(cubeReg, (float) Cube.getX1(), 2 * sizeBlock,
					(float) (sizeBlock / 2), (float) (sizeBlock / 2),
					(float) sizeBlock, sizeBlock, (float) 1, (float) 1,
					(float) Cube.getA());

			game.batch.draw(FrontText, 0, 0);
			if ((showMess) && (cur_level[x_m][y_m].isShowMess() == false)) {
				game.batch.draw(cur_level[x_m][y_m].getMessText(), 0, 0,
						Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
			}
			game.batch.end();
		}
	}

	public boolean checkInters(int x_c, int y_c) {
		if (Intersector.intersectRectangles(Cube.getRectangle(),
				cur_level[x_c][y_c].getRectangle(), new Rectangle())) {
			return true;
		}
		return false;
	}

	public boolean checkInScreen(int x_c, int y_c) {
		if ((cur_level[x_c][y_c].getRectangle().x > 0)
				&& (cur_level[x_c][y_c].getRectangle().x < Gdx.graphics
						.getWidth())) {
			return true;
		}
		return false;
	}

	public boolean checkEquals(int x_c, int y_c) {
		if ((cur_level[x_c][y_c].getRectangle().x == Cube.getRectangle().x)
				&& (cur_level[x_c][y_c].getRectangle().y == Cube.getRectangle().y)) {
			return true;
		}
		return false;
	}

	private void update(Persons body) {
		int x_c, y_c;

		if (lastGod != god) {
			if (god) {
				System.out.println("God Mode:On. Have Fun!");
			} else {
				System.out.println("God Mode:Off. So sad:(");
			}
		}
		lastGod = god;
		for (x_c = 0; x_c < x; x_c++) {
			for (y_c = 0; y_c < y; y_c++) {
				// System.out.print("yEs");
				if ((cur_level[x_c][y_c].getType() == 6)
						&& (checkInters(x_c, y_c))) {
					if (this.level + 1 <= 10) {
						try {
							game.setScreen(new GameScreen(game, this.world,
									this.level + 1));
						} catch (NumberFormatException e) {
						} catch (IOException e) {
						}
					} else {
						if (this.world + 1 <= 5) {
							try {
								game.setScreen(new GameScreen(game,
										this.world + 1, 1));
							} catch (NumberFormatException e) {
							} catch (IOException e) {
							}
						} else {
							System.out.println("End levels");
						}
					}
				}
				if (checkInScreen(x_c, y_c)) {
					// System.out.print("Yes");
					if ((checkInters(x_c, y_c)) && (checkEquals(x_c, y_c))) {
						if (cur_level[x_c][y_c].getType() == 4) {
							body.setVelo(cur_level[x_c][y_c].getVelo());
							break;
							// System.out.print("YEs");
							// updateY((-1)*cur_level[x_c][y_c].getVelo());
						}
					}
					if ((checkEquals(x_c, y_c))
							&& (cur_level[x_c][y_c].getType() == 5)
							&& (god == false)) {
						parseStart();
					}
					if (cur_level[x_c][y_c].getType() == 10) {
						if (checkInters(x_c, y_c)) {
							showMess = true;
							x_m = x_c;
							y_m = y_c;
							break;
						} else {
							showMess = false;
						}
					}
				}
			}
		}

		if (Cube.getVelo() > 0) {
			updateY((-1) * (int) (sizeBlock / 5));
			Cube.setVelo(Cube.getVelo() - (int) (sizeBlock / 5));
			// curGrav=0;
			// gravitation=false;
		} else {

			gravity(body);
			if ((gravitation) && (curGrav != 2)) {
				updateY((int) (sizeBlock / 2));
				curGrav++;
			} else {

				gravitation = false;
				curGrav = 0;
			}
		}
		if ((step != 0) && (curStep != 10)) {
			wall(Cube, step);
			curStep += 1;

			if ((upWall) && (curUp != 5) && (moveWall)) {
				curUp++;
				updateY((-1) * (int) (sizeBlock / 5));
			} else {
				upWall = false;
				curUp = 0;
			}
			if (moveWall) {
				updateX(step * 5);
				Cube.Make_Iteration(step * (-1));

			}
		} else {
			upWall = false;
			currentX -= step;
			// System.out.print(currentX);
			// gravity(Cube);
			if (up == 0) {
				step = 0;
				curStep = 0;
			} else {
				curStep = 0;
			}
		}
	}

	private void parseStart() {
		int x_c, y_c;
		for (x_c = 0; x_c < x; x_c++) {
			for (y_c = 0; y_c < y; y_c++) {
				if (cur_level[x_c][y_c].getType() == 3) {
					currentX = x_c;
					currentY = y_c;
					x_s = (int) cur_level[x_c][y_c].getRectangle().x
							- (int) (2 * sizeBlock);
					y_s = (int) cur_level[x_c][y_c].getRectangle().y
							- (int) (2 * sizeBlock);
					break;
				}
			}
		}
		updateX((-1) * x_s);
		updateY((-1) * y_s);
		backgroundRect = new Rectangle(0, 0, Gdx.graphics.getWidth(),
				Gdx.graphics.getHeight());
		backgroundRect2 = new Rectangle(Gdx.graphics.getWidth(), 0,
				Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

	}

	private boolean wall(Persons body, int vector) {
		int x_c, y_c;
		for (x_c = 0; x_c < x; x_c++) {
			for (y_c = 0; y_c < y; y_c++) {
				if (checkInScreen(x_c, y_c)) {
					if (Cube.isHaveKey()) {
						if ((Math.abs(cur_level[x_c][y_c].getRectangle().y
								- body.getRectangle().y) <= sizeBlock + 1)
								&& (Math.abs(cur_level[x_c][y_c].getRectangle().x
										+(vector)* body.getRectangle().x) <= sizeBlock + 1)) {
							if (cur_level[x_c][y_c].checkKey(Cube.getKey())) {
								cur_level[x_c][y_c].setType(0);
								Cube.setHaveKey(false);
							}
						}
					}
					if ((cur_level[x_c][y_c].getRectangle().x + sizeBlock
							* vector == body.getRectangle().x)
							&& (cur_level[x_c][y_c].getRectangle().y == body
									.getRectangle().y)
							&& ((cur_level[x_c][y_c].getType() == 1) || (cur_level[x_c][y_c]
									.getType() == 2))) {
						// System.out.println(x_c);
						if ((y_c - 1 >= 0)
								&& ((cur_level[x_c][y_c - 1].getType() == 1) || (cur_level[x_c][y_c - 1]
										.getType() == 2))) {
							// System.out.println(vector);
							upWall = false;
							moveWall = false;
							break;
						}
						if ((x_c + vector >= 0)
								&& (y_c - 1 >= 0)
								&& ((cur_level[x_c + vector][y_c - 1].getType() == 1) || (cur_level[x_c
										+ vector][y_c - 1].getType() == 2))) {
							// System.out.println(x_c);
							upWall = false;
							moveWall = false;
							break;
						}

						upWall = true;
						moveWall = true;
						break;

					}

				}
			}
		}

		return false;
	}

	private boolean gravity(Persons body) {
		int x_c, y_c;
		for (x_c = 0; x_c < x; x_c++) {
			for (y_c = 0; y_c < y; y_c++) {
				if (checkInScreen(x_c,y_c)) {
					if ((Cube.isHaveKey() == false)
							&& (curStep == 0)
							&& (upWall == false)
							&& (checkInters(x_c,y_c))) {
						if (cur_level[x_c][y_c].getKey()) {
							Cube.setKey(cur_level[x_c][y_c].getItemKey());
							cur_level[x_c][y_c].setType(0);
							Cube.setHaveKey(true);
						}
					}
					if ((curStep == 0)
							&& (upWall == false)
							&& (gravitation == false)
							&& (Intersector.intersectRectangles(
									cur_level[x_c][y_c].getRectangle(), body
											.getRectangle(), new Rectangle(0,
											0, 0, 0)))) {
						if (cur_level[x_c][y_c].isPortal()) {
							updateX((1) * (int) (cur_level[x_c][y_c]
									.getPortalRect().x - (int) (2 * sizeBlock)));
							updateY((1) * (int) (cur_level[x_c][y_c]
									.getPortalRect().y - (int) (2 * sizeBlock)));
							break;
						}
					}
					if ((cur_level[x_c][y_c].getRectangle().x == body
							.getRectangle().x)
							&& (cur_level[x_c][y_c].getRectangle().y
									+ sizeBlock == body.getRectangle().y)
							&& (cur_level[x_c][y_c].getType() != 1)
							&& (cur_level[x_c][y_c].getType() != 2)) {
						gravitation = true;
					}
				}
			}
		}

		return true;
	}

	public void updateX(int currentX) {
		int x_c, y_c;
		for (x_c = 0; x_c < x; x_c++) {
			for (y_c = 0; y_c < y; y_c++) {
				cur_level[x_c][y_c].changeX(currentX);
			}
		}
		if (backgroundRect.x + currentX > Gdx.graphics.getWidth()) {
			backgroundRect.x = backgroundRect2.x - backgroundRect2.width;
		}
		if (backgroundRect2.x + currentX > Gdx.graphics.getWidth()) {
			backgroundRect2.x = backgroundRect.x - backgroundRect.width;
		}
		if (backgroundRect.x + currentX < (-1) * (Gdx.graphics.getWidth())) {
			backgroundRect.x = backgroundRect2.x + backgroundRect2.width;
		}
		if (backgroundRect2.x + currentX < (-1) * (Gdx.graphics.getWidth())) {
			backgroundRect2.x = backgroundRect.x + backgroundRect.width;
		}
		backgroundRect.x += currentX;
		backgroundRect2.x += currentX;
		// }
	}

	public void updateY(int currentY) {
		int x_c, y_c;

		for (x_c = 0; x_c < x; x_c++) {
			for (y_c = 0; y_c < y; y_c++) {
				cur_level[x_c][y_c].changeY(currentY);
			}
		}

	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void show() {
		// rainMusic.play();
	}

	@Override
	public void hide() {
		// rainMusic.stop();
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {

	}

	class Input implements InputProcessor {
		String godDeam = "";

		@Override
		public boolean keyDown(int keycode) {
			if (keycode == Keys.G) {
				godDeam = "g";
			}
			if ((keycode == Keys.O) && (godDeam == "g")) {
				godDeam += "o";
			}
			if ((keycode == Keys.D)
					&& ((godDeam.equals("go")) || (godDeam.equals("god")))) {
				godDeam += "d";
			}
			if ((keycode == Keys.E) && (godDeam.equals("godd"))) {
				godDeam += "e";
			}
			if ((keycode == Keys.A) && (godDeam.equals("godde"))) {
				godDeam += "a";
			}
			if ((keycode == Keys.M) && (godDeam.equals("goddea"))) {
				godDeam += "m";
			}
			// System.out.println(godDeam);
			if (godDeam.equals("goddeam")) {
				if (god) {
					god = false;
				} else {
					god = true;
				}
				godDeam = "";
			}
			return false;
		}

		@Override
		public boolean keyUp(int keycode) {
			if (keycode == Keys.R) {
				parseStart();
			}
			return false;
		}

		@Override
		public boolean keyTyped(char character) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean touchDown(int screenX, int screenY, int pointer,
				int button) {
			// Fix bag (1)----------------------------------
			screenX = Math.round(screenX * width);
			screenY = Math.round(screenY * heigth);
			// ----------------------------------------------

			if (800 / screenX < 2) {
				// updateX(-50);
				if (step == 0) {
					step = -1;
					moveWall = true;
					gravity(Cube);
				}
				up = 1;
			} else {
				// updateX(50);
				if (step == 0) {
					step = 1;
					moveWall = true;
					gravity(Cube);
				}
				up = 1;
			}

			return false;
		}

		@Override
		public boolean touchUp(int screenX, int screenY, int pointer, int button) {
			/*
			 * int i; // Fix bag (1)----------------------------------
			 * screenX=Math.round(screenX*width);
			 * screenY=Math.round(screenY*heigth);
			 * //----------------------------------------------
			 * if(800/screenX<2){ //updateX(-50); step=-1; }else{ //updateX(50);
			 * step=1; }
			 */
			up = 0;
			return false;
		}

		@Override
		public boolean touchDragged(int screenX, int screenY, int pointer) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean mouseMoved(int screenX, int screenY) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean scrolled(int amount) {
			// TODO Auto-generated method stub
			return false;
		}

	}

}
