package com.cubes;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
 /*	This class controls Screen.
  *	WARNING this class not for edit
  *	You not see this class...never
  */
public class Drop extends Game {
	SpriteBatch batch;
	BitmapFont font;
	Music fonMusic;
	String levelStr,currentStr;
	boolean musicEnable=true,soundEnable=true;
	public void create() {
		batch = new SpriteBatch();
		font = new BitmapFont();
		fonMusic= Gdx.audio.newMusic(Gdx.files.internal("Game/backgroundMusic.mp3"));
		fonMusic.setLooping(true);
		
		
		FileHandle file_level = Gdx.files.local("Game/game.conf");
		levelStr = file_level.readString();
		System.out.println(levelStr);
		getNextString();
		musicEnable=Boolean.parseBoolean(currentStr);
		
		
		getNextString();
		soundEnable=Boolean.parseBoolean(currentStr);
		
		if(musicEnable){
			fonMusic.play();
		}
	
	
	
		this.setScreen(new MainMenuScreen(this,musicEnable,soundEnable));
		
	}
	public void getNextString() {
	String time;
	int i, j;
	time = "";
	i = 0;
	if (levelStr.length() != 0) {
		while (levelStr.charAt(i) != 'e') {
			time += levelStr.charAt(i);
			i++;
		}
		time+=levelStr.charAt(i);
		i++;
	}
	currentStr = time;
	time = "";
	for (j = i; j < levelStr.length(); j++) {
		time += levelStr.charAt(j);
	}
	levelStr = time;
}
	public void render() {
		super.render(); 
	}
	public boolean haveMusic(){
		return musicEnable;
	}
	public void setMusic(boolean musicEnable){
		this.musicEnable=musicEnable;
		if(musicEnable){
			fonMusic.play();
		}else{
			fonMusic.stop();
		}
	}
	public void dispose() {
		batch.dispose();
		font.dispose();
	}
}