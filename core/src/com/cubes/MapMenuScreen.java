package com.cubes;
 
import java.io.IOException;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
/*
  * This class draw menu for load levels
  */
public class MapMenuScreen implements Screen {
  final Drop game;
  	//global parametrs-------------------------
	Texture background_Texture;
	Texture WorldText;
	Texture loadText;
	
	Sound dropSound;
	Music rainMusic;
	
	OrthographicCamera camera;
	Rectangle bucket;
	Rectangle loadRect;
	int Change;
	
	int Xpress,Ypress;
	int numLoad=0;

	float width;
	float heigth;
	
	int currentWorld=0;
	
	boolean load=false;

	//Get and Set for last coordinate
	public int getXpress() {
		return Xpress;
	}


	public void setXpress(int xpress) {
		Xpress = xpress;
	}


	public int getYpress() {
		return Ypress;
	}


	public void setYpress(int ypress) {
		Ypress = ypress;
	}
	//------------------------------------------------
	public BlockMap[] World=new BlockMap[6];
	BlockMap[] Level=new BlockMap[11];
	//----------------------------------------------
	public MapMenuScreen(final Drop gam) {
		this.game = gam;
			loadText=new Texture("Game/loading.jpeg");
			loadRect=new Rectangle(
					(-1)*Math.abs((loadText.getWidth()-800)/2)
					,(-1)*Math.abs((loadText.getHeight()-600)/2)
					,loadText.getWidth(),loadText.getHeight());
			//Connect camera-------------------------------------------
			camera = new OrthographicCamera();
			camera.setToOrtho(false, 800, 600);
			//--------------------------------------------------------------
			
		
		
	}
	private void startGame(int World,int Level){
		try {
			game.setScreen(new GameScreen(game,World,Level));
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private void loading(){
		int i;
			//Load Texture--------------------------------------------------------------
			background_Texture = new Texture(Gdx.files.internal("MapMenu/menu.png"));
			//--------------------------------------------------------------------------
			//Connect camera----------------------------------------------------------
			camera = new OrthographicCamera();
			camera.setToOrtho(false, 800, 600);
			//-----------------------------------------------------------------------
			//Bag (1) fix--------------------------------------------------------------
			width=(float) (800.0/Gdx.graphics.getWidth());
			heigth=(float) (600.0/Gdx.graphics.getHeight());
			//----------------------------------------------------------------------
			//Load array object's
			for(i=1;i<6;i++){
					WorldText=new Texture(Gdx.files.internal("MapMenu/world"+i+".png"));
					World[i]=new BlockMap(WorldText, new Rectangle(800*(i-1),0,800,600));
				}
			for(i=1;i<11;i++){
					WorldText=new Texture(Gdx.files.internal("MapMenu/level"+i+".png"));
					Level[i]=new BlockMap(WorldText, new Rectangle(800*(i-1),0,800,600));
				}
			//-----------------------------------------------------
			load=true;
			Gdx.input.setInputProcessor(new Input()); 
	}
	@Override
	public void render(float delta) {
		int i;
		Gdx.gl.glClearColor(0, 0, 0.2f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
 
			if(numLoad<90){
					if((load==false)&&(numLoad>0)){
							loading();
						}
					game.batch.setProjectionMatrix(camera.combined);
					camera.update();

					game.batch.begin();
					game.batch.draw(loadText, loadRect.x,loadRect.y,loadRect.width,loadRect.height);
					game.batch.end();

					numLoad++;
				}else{
		camera.update();
 
		
		game.batch.setProjectionMatrix(camera.combined);
		game.batch.begin();
		game.batch.draw(background_Texture,0,0,800,600);
		if(currentWorld==0){
		for(i=1;i<6;i++){
			game.batch.draw(World[i].getTexture(),World[i].getRectangle().x,
					World[i].getRectangle().y, World[i].getRectangle().width,World[i].getRectangle().height);
			}
		}else{
			for(i=1;i<11;i++){
				game.batch.draw(Level[i].getTexture(),Level[i].getRectangle().x,
						Level[i].getRectangle().y, Level[i].getRectangle().width,Level[i].getRectangle().height);
				}
		}
		game.batch.end();
		
		}
		
		
	}
	private void Changes(int x,boolean world){
		int i;
		if(world){
		if((x<0)||(World[1].getRectangle().x+x<0)){
			if(World[5].getRectangle().x+x>0){
		for(i=1;i<6;i++){
			World[i].setX_Y(x);
		
		}
			}
		}
		}else{
			if((x>0)&&(Level[1].getRectangle().x==0)){}
			else{
				if(Level[10].getRectangle().x+x>0){
			for(i=1;i<11;i++){
				Level[i].setX_Y(x);
			
			}
				}
			}
		}
	}
	@Override
	public void resize(int width, int height) {
	}
 
	@Override
	public void show() {
		// start the playback of the background music
		// when the screen is shown
		//rainMusic.play();
	}
 
	@Override
	public void hide() {
	}
 
	@Override
	public void pause() {
	}
 
	@Override
	public void resume() {
	}
 
	@Override
	public void dispose() {
		int i;
		for(i=1;i<6;i++){
		World[i].getTexture().dispose();
		}
		for(i=1;i<11;i++){
			
			Level[i].getTexture().dispose();
		}
	}

	class Input implements InputProcessor{
		int i,x,y,p_x,p_y;
		
		@Override
		public boolean keyDown(int keycode) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean keyUp(int keycode) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean keyTyped(char character) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean touchDown(int screenX, int screenY, int pointer, int button) {
			screenX=Math.round(screenX*width);
			screenY=Math.round(screenY*heigth);
			if((screenX>0)&&(screenY>0)){
			setXpress(screenX);
			setYpress(screenY);
			}
			
			return false;
		}

		@Override
		public boolean touchUp(int screenX, int screenY, int pointer, int button) {
			int i;
			screenX=Math.round(screenX*width);
			screenY=Math.round(screenY*heigth);
			if(currentWorld==0){
			for(i=1;i<6;i++){	
			if((screenX>World[i].getRectangle().x)&&(screenX<World[i].getRectangle().x+World[i].getRectangle().width)
					&&(Math.abs(screenX-Xpress)<10)&&(Math.abs(screenY-Ypress)<10)){
					currentWorld=i;
				break;
			}
			}
			}else{
				for(i=1;i<11;i++){	
					if((screenX>Level[i].getRectangle().x)&&(screenX<Level[i].getRectangle().x+Level[i].getRectangle().width)
							&&(Math.abs(screenX-Xpress)<10)&&(Math.abs(screenY-Ypress)<10)){
							startGame(currentWorld,i);
						break;
					}
					}
			}
			return false;
		}

		@Override
		public boolean touchDragged(int screenX, int screenY, int pointer) {
			int timeSleep=3;
			boolean bol;
			int i;
			screenX=Math.round(screenX*width);
			screenY=Math.round(screenY*heigth);
			if(currentWorld==0){
				bol=true;
			} else{
				bol=false;
			}
			if(screenX<x){
				Changes(-20,bol);
				for(i=-5;i<0;i++){
					Changes(i,bol);
					try {
						Thread.sleep(timeSleep);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			else{
				Changes(+20,bol);
				for(i=5;i>0;i--){
					Changes(i,bol);
					try {
						Thread.sleep(timeSleep);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			x=screenX;
			return false;
		}

		@Override
		public boolean mouseMoved(int screenX, int screenY) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean scrolled(int amount) {
			
			return false;
		}
	}
 
}
class BlockMap{
	Texture texture;
	Rectangle rectangle;
	int x,y;
	
	public Texture getTexture() {
		return texture;
	}


	public void setTexture(Texture texture) {
		this.texture = texture;
	}

	public void setX_Y(int x){
		rectangle.x+=x;
		
			
	}
	public Rectangle getRectangle() {
		return rectangle;
	}
	

	public void setRectangle(Rectangle rectangle) {
		this.rectangle = rectangle;
	}


	BlockMap(Texture text,Rectangle rect){
		setTexture(text);
		setRectangle(rect);
	}	
	
	
	
}

