package com.cubes;

import java.io.Console;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
 /*
  *  Class otve4et za options
  */
public class OptionScreen implements Screen {
 
	//Global parametr----------------------------------
	final Drop game;
	OrthographicCamera camera;
	
	Texture menu;
	Texture btnSound;
	Texture btnMusic;
	Texture steering;
	Texture btnSteering;
	Texture btnBack;
	
	Rectangle btnRectSound;
	Rectangle bnRectMusic;
	Rectangle btnRectSteering;
	Rectangle btnRectBack;
	
	TextureRegion[][] Sound;
	boolean enableSound=true;
	
	TextureRegion[][] Music;
	boolean enableMusic=true;
	
	TextureRegion[][] Stree;
	boolean enableStree=true;
	
	float width;
	float heigth;
	String levelStr,currentStr;
	FileHandle file_level;
	//------------------------------------------------------------------
	public OptionScreen(final Drop gam,boolean confSound,boolean confMusic) {
		game = gam;
		//Load Textute---------------------------------------------
		enableSound=confSound;
		enableMusic=confMusic;
		menu=new Texture("options/menu.png");
		btnSound=new Texture("options/On_Off_Sound.png");
		btnMusic=new Texture("options/On_Off_Music.png");
		steering=new Texture("options/Steering.png");
		btnSteering=new Texture("options/Steering_opt.png");
		btnBack=new Texture("options/Back.png");
		//--------------------------------------------------------------
		// Load and create Animation--------------------------------------------
		Sound= TextureRegion.split(btnSound, btnSound.getWidth()/2, btnSound.getHeight());
		Music= TextureRegion.split(btnMusic, btnMusic.getWidth()/2, btnMusic.getHeight());
		Stree= TextureRegion.split(btnSteering, btnSteering.getWidth()/2, btnSteering.getHeight());
		//--------------------------------------------------------------
		//Install parameterss Rectangle's-----------------------------
		btnRectSound=new Rectangle(100,350,420,200);
		bnRectMusic=new Rectangle(500,350,420,200);
		btnRectSteering=new Rectangle(450,170,400,150);
		btnRectBack=new Rectangle(200,20,350,120);
		//--------------------------------------------------------------
		// Fix bag (1)-------------------------
		width=(float) (800.0/Gdx.graphics.getWidth());
		heigth=(float) (600.0/Gdx.graphics.getHeight());
		//--------------------------------------------------------------
		//Connect camera-------------------------------------------
		camera = new OrthographicCamera();
		camera.setToOrtho(false, 800, 600);
		//--------------------------------------------------------------
		//Input class, he listening touch or click mouse ------------------------
		Gdx.input.setInputProcessor(new Input()); 
		//--------------------------------------------------------------
		
		
	}
	@Override
	public void render(float delta) {
		//Change parametrs options-------------------------------
		int curMusic=1;
		int curSound=1;
		int curStree=1;
		if (enableSound){
			curSound=0;
		}
		if (enableStree){
			curStree=0;
		}
		if (enableMusic){
			curMusic=0;
		}
		//------------------------------------------------
		Gdx.gl.glClearColor(0, 0, 0.2f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
 
		camera.update();
		game.batch.setProjectionMatrix(camera.combined);
		//Draw texture------------------------------------------
		//If it's animation's draw one of two cadr's
		game.batch.begin();
		game.batch.draw(menu,0,0,800,600);
		game.batch.draw(Sound[0][curSound],btnRectSound.x,btnRectSound.y,btnRectSound.width/2,btnRectSound.height);
		game.batch.draw(Music[0][curMusic],bnRectMusic.x,bnRectMusic.y,bnRectMusic.width/2,bnRectMusic.height);
		game.batch.draw(Stree[0][curStree],btnRectSteering.x,btnRectSteering.y,btnRectSteering.width/2,btnRectSteering.height);
		game.batch.draw(steering, 25, 170, 400,150);
		game.batch.draw(btnBack,btnRectBack.x,btnRectBack.y,btnRectBack.width,btnRectBack.height);
		game.batch.end();
		//End draw-----------------------------------------------------------------------------
 
		
	}
 
	@Override
	public void resize(int width, int height) {
	}
 
	@Override
	public void show() {
		
	}
 
	@Override
	public void hide() {
	}
 
	@Override
	public void pause() {
	}
 
	@Override
	public void resume() {
	}
 
	@Override
	public void dispose() {
	}
	/*
	 * He's listening
	 */
	class Input implements InputProcessor{

		@Override
		public boolean keyDown(int keycode) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean keyUp(int keycode) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean keyTyped(char character) {
			// TODO Auto-generated method stub
			return false;
		}
	
		@Override
		public boolean touchDown(int screenX, int screenY, int pointer,int button) {
			//Fix bag (1)------------------------------
			screenX=Math.round(screenX*width);
			screenY=Math.round(screenY*heigth);
			screenY=600-screenY;
			//-----------------------------------------
			if((screenX>btnRectSound.x)&&(screenX<btnRectSound.x+btnRectSound.width/2)&&
					(screenY>btnRectSound.y)&&(screenY<btnRectSound.y+btnRectSound.height)){
				if(enableSound){
					enableSound=false;
				
				}else{
					enableSound=true;
					
				}
			}
			if((screenX>bnRectMusic.x)&&(screenX<bnRectMusic.x+bnRectMusic.width/2)&&
					(screenY>bnRectMusic.y)&&(screenY<bnRectMusic.y+bnRectMusic.height)){
				if(enableMusic){
					enableMusic=false;
					game.setMusic(false);
				}else{
					enableMusic=true;
					game.setMusic(true);
				}
			}
			
			if((screenX>btnRectSteering.x)&&(screenX<btnRectSteering.x+btnRectSteering.width/2)&&
					(screenY>btnRectSteering.y)&&(screenY<btnRectSteering.y+btnRectSteering.height)){
				if(enableStree){
					enableStree=false;
				}else{
					enableStree=true;
				}
			}
			if((screenX>btnRectBack.x)&&(screenX<btnRectBack.x+btnRectBack.width)&&
					(screenY>btnRectBack.y)&&(screenY<btnRectBack.y+btnRectBack.height)){
				FileHandle file_level = Gdx.files.local("Game/game.conf");
				if(enableMusic){
				file_level.writeString("true", false);
				}else
				{
					file_level.writeString("false", false);
				}
				file_level.writeString("/n", true);
				if(enableSound){
					file_level.writeString("true", true);
					}else
					{
						file_level.writeString("false", true);
					}
				file_level.writeString(" ", true);
				
				game.setScreen(new MainMenuScreen(game,enableSound,enableMusic));
			}
			return false;
		}

		@Override
		public boolean touchUp(int screenX, int screenY, int pointer, int button) {
			
			
			return false;
		}

		@Override
		public boolean touchDragged(int screenX, int screenY, int pointer) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean mouseMoved(int screenX, int screenY) {
			
			return false;
		}

		@Override
		public boolean scrolled(int amount) {
			// TODO Auto-generated method stub
			return false;
		}

		
		
	}
}