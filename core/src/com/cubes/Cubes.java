package com.cubes;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;

public class Cubes {
	Texture texture;
	Rectangle rectangle;
	float angel;
	int velo;

	Cubes(Texture texture, Rectangle rectangle, float angel) {
		this.texture = texture;
		this.rectangle = rectangle;
		this.angel = angel;
	}

	public Texture getTexture() {
		return texture;
	}

	public void setVelo(int velo) {
		this.velo = velo;
	}

	public int getVelo() {
		return velo;
	}

	public void setTexture(Texture texture) {
		this.texture = texture;
	}

	public Rectangle getRectangle() {
		return rectangle;
	}

	public void setRectangle(Rectangle rectangle) {
		this.rectangle = rectangle;
	}

	public float getAngel() {
		return angel;
	}

	public void setAngel(float angel) {
		this.angel = angel;
	}

}