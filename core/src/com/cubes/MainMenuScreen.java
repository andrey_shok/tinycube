package com.cubes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
 /*
  *  Load and draw MENU
  */
public class MainMenuScreen implements Screen {
	Persons persons;
	//global parametrs----------------------------------
	final Drop game;
	OrthographicCamera camera;
	Texture menu;
	Texture btnMenu;
	Texture btnOptions;
	Texture loadText;
	Boolean Active=true;
	Rectangle btnMenuRect;
	Rectangle btnOptionsRect;
	Rectangle loadRect;
	float width;
	float heigth;
	boolean load=false;
	boolean musicEnable=true,soundEnable=true;
	int numLoad=0;
	//------------------------------------------------------------------
	public MainMenuScreen(final Drop gam,boolean confSound,boolean confMusic) {
		game = gam;
		musicEnable=confMusic;
		soundEnable=confSound;
			loadText=new Texture("Game/loading.jpeg");
			loadRect=new Rectangle(
					(-1)*Math.abs((loadText.getWidth()-800)/2)
					,(-1)*Math.abs((loadText.getHeight()-600)/2)
					,loadText.getWidth(),loadText.getHeight());
			//Connect camera-------------------------------------------
			camera = new OrthographicCamera();
			camera.setToOrtho(false,800, 600);
			//--------------------------------------------------------------
		
	}
	
   private void loading(){
		   //Load Texture---------------------------------------------
		   menu=new Texture("menu/menu.png");
		   btnMenu=new Texture("menu/start.png");
		   btnOptions=new Texture("menu/options.png");
		   //--------------------------------------------------------------
		   //Install Rectangle -----------------------------
		   btnMenuRect=new Rectangle(10,400,490,140);
		   btnOptionsRect=new Rectangle(300,260,490,140);
		   //--------------------------------------------------------------
		 
		   // Fix bag(1)-------------------------
		   width=(float) (800.0/Gdx.graphics.getWidth());
		   heigth=(float) (600.0/Gdx.graphics.getHeight());
		   //--------------------------------------------------------------
		   //Listner input process------------------------
		   Gdx.input.setInputProcessor(new Input()); 
		   //--------------------------------------------------------------
		   TextureRegion text=new TextureRegion();
		   text.setRegion(new Texture("Game/Cube.png"));
		   
		   load=true;
   }
	@Override
	public void render(float delta) {
		
		Gdx.gl.glClearColor(0, 0, 0.2f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		if(numLoad<90){
			if((load==false)&&(numLoad>0)){
				loading();
			}
				game.batch.setProjectionMatrix(camera.combined);
				camera.update();
				
				game.batch.begin();
				game.batch.draw(loadText, loadRect.x,loadRect.y,loadRect.width,loadRect.height);
				game.batch.end();
			
			numLoad++;
		}else{
	
				game.batch.setProjectionMatrix(camera.combined);
				camera.update();
		game.batch.begin();
		game.batch.draw(menu,0,0,800,600);
		game.batch.draw(btnMenu,btnMenuRect.x,btnMenuRect.y,btnMenuRect.width,btnMenuRect.height);
		game.batch.draw(btnOptions,btnOptionsRect.x,btnOptionsRect.y,btnOptionsRect.width,btnOptionsRect.height);
		game.batch.end();
 			}
		
	}
 
	@Override
	public void resize(int width, int height) {
	}
 
	@Override
	public void show() {
		
	}
 
	@Override
	public void hide() {
	}
 
	@Override
	public void pause() {
	}
 
	@Override
	public void resume() {
	}
 
	@Override
	public void dispose() {
	Active=false;
		 menu.dispose();
		 btnMenu.dispose();
		 btnOptions.dispose();
	}
	  		
	public Boolean getActive() {
		return Active;
	}


	public void setActive(Boolean active) {
		Active = active;
	}

	/*
	 * Listner 
	 */
	class Input implements InputProcessor{

		@Override
		public boolean keyDown(int keycode) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean keyUp(int keycode) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean keyTyped(char character) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean touchDown(int screenX, int screenY, int pointer,int button) {
			// Fix bag (1)----------------------------------
			screenX=Math.round(screenX*width);
			screenY=Math.round(screenY*heigth);
			//----------------------------------------------
			if(Active){
			screenY=600-screenY;
			if((screenX>btnMenuRect.x)&&(screenX<btnMenuRect.x+btnMenuRect.width)&&
					(screenY>btnMenuRect.y)&&(screenY<btnMenuRect.y+btnMenuRect.height)){
				game.setScreen(new MapMenuScreen(game));
				dispose();
			}
			if((screenX>btnOptionsRect.x)&&(screenX<btnOptionsRect.x+btnOptionsRect.width)&&
					(screenY>btnOptionsRect.y)&&(screenY<btnOptionsRect.y+btnOptionsRect.height)){
				game.setScreen(new OptionScreen(game,musicEnable,soundEnable));
				dispose();
			}
			
			}
			return false;
		}

		@Override
		public boolean touchUp(int screenX, int screenY, int pointer, int button) {
			
			
			return false;
		}

		@Override
		public boolean touchDragged(int screenX, int screenY, int pointer) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean mouseMoved(int screenX, int screenY) {
			
			return false;
		}

		@Override
		public boolean scrolled(int amount) {
			// TODO Auto-generated method stub
			return false;
		}

		
		
	}
}
