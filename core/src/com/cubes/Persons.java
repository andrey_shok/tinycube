package com.cubes;
import com.cubes.*;
import java.io.BufferedReader;
import java.io.IOException;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;
 /*	Данный класс есть cборище всеяких гадов:
  * монстрики, хорошие монстрики и сам куб.
  * Короче типа круто...
  */
public class Persons {

	
  Rectangle rectangle;
  private double a;//текущий угол
  private double W;//текущая угловая скорость
  private double W0=500;//начальная угловая скорость
  private double a0=0;//начальный угол
  private double dt=0.018;//период подсчета
  private double t;//время
  private double x0, y0;//координаты нижней левой точки rectangle
  private double x1, y1,x2;//координаты точки, которая вращается
  private int velo;
  private char key;
  private boolean haveKey=false;
		
		/*
		 *  Конструктор класса.
		 */
  	
		//get/set
		
		//moving
  		Persons(Rectangle rectangle,float angel){
			this.rectangle=rectangle;
			this.a0=angel;
			W=W0;
			a=a0;
			a=a0;
			x0=rectangle.x;
			y0=rectangle.y;
			x1=x0;
			y1=y0;
  		}
  		public boolean isHaveKey() {
			return haveKey;
		}
		public void setHaveKey(boolean haveKey) {
			this.haveKey = haveKey;
		}
		public void setVelo(int velo){
			this.velo=velo;
		}
		public int getVelo(){
			return velo;
		}
		
		public char getKey() {
			return key;
		}
		public void setKey(char key) {
			this.key = key;
		}
		public void Move(int vector)
		{
			//setRectangle(rectangle);//???
			//setTexture(texture);//???
			W=W0;
			a=a0;
			x0=rectangle.x;
			y0=rectangle.y;
			x1=x0;
			y1=y0;
			//vector -1=left  1=right;
			Make_Iteration(vector);
			//Thread.sleep(dt/1000.0);
			
		}
		public void Make_Iteration(int vector)
		{
			
			t=t+dt;
			double g=9.81;//free falling acceleration
			double side=0.5;//side of cube
			W=Math.sqrt(  Math.pow(W0, 2.0)-6*g*Math.sqrt(2)*Math.cos(Math.PI/4-a)/side );
			if (vector>=1)
			{
				a=a-W*dt;
				x0++;
			}
			if (vector<0)
			{
				a=a+W*dt;
				x0--;
			}
			//x1=x0+side-side*(Math.sin(a));
			//x2++;
			y1=y0+side-side*(Math.cos(a));
			
		}
		
		
		public Rectangle getRectangle() {
			return rectangle;
		}

		public void setRectangle(Rectangle rectangle) {
			this.rectangle = rectangle;
		}

		public double getA() {
			return a;
		}

		public void setA(double a) {
			this.a += a;
		}

		public double getX1() {
			return x1;
		}

		public void setX1(double x1) {
			this.x1 = x1;
		}

		public double getY1() {
			return y1;
		}

		public void setY1(double y1) {
			this.y1 = y1;
		}

		
			//game.batch.draw(texture, (float)x1, (float)y1, (float) 25, (float)25 , (float)rectangle.width, (float)rectangle.height,(float)1, (float)1, (float)a);
}