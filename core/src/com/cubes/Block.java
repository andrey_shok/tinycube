package com.cubes;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;

public class Block {
	Texture texture;
	Rectangle rectangle, portalRect;
	char keyArray;
	char keyCur;
	int Type, velo, index = 0;
	Texture messText;
	float angle = 0f;
	boolean showMess = false, portal = false, keyItem = false;

	public void setItemKey(char key) {
		this.keyCur = key;
		keyItem = true;
	}

	public float getAngle() {
		return angle;
	}

	public void setAngle(float angle) {
		this.angle = angle;
	}

	public char getItemKey() {
		keyItem = false;
		return this.keyCur;

	}

	public boolean getKey() {
		return keyItem;
	}

	public void addKey(char key) {
		keyArray = key;

	}

	public boolean checkKey(char key) {
		if (keyArray == key) {
			return true;
		}
		return false;
	}

	public boolean isPortal() {
		return portal;
	}

	public void setPortal(boolean portal) {
		this.portal = portal;
	}

	public Rectangle getPortalRect() {
		return portalRect;
	}

	public void setPortalRect(Rectangle portalRect) {
		this.portalRect = portalRect;
	}

	public boolean isShowMess() {
		return showMess;
	}

	public void setShowMess(boolean showMess) {
		this.showMess = showMess;
	}

	public Texture getMessText() {
		return messText;
	}

	public void setMessText(Texture messText) {
		this.messText = messText;
	}

	Block(int typeBlocks, Rectangle rectangle, Texture texture, int velo) {
		this.rectangle = rectangle;
		this.Type = typeBlocks;
		this.texture = texture;
		this.velo = velo;
	}

	public void setTexture(Texture texture) {
		this.texture = texture;
	}

	public void setType(int type) {
		this.Type = type;
	}

	public void setVelo(int velo) {
		this.velo = velo;
	}

	public int getType() {
		return Type;
	}

	public Texture getText() {
		return this.texture;
	}

	public int getVelo() {
		return velo;
	}

	public Rectangle getRectangle() {
		return rectangle;
	}

	public void changeX(int x) {
		rectangle.x += x;
	}

	public void changeY(int y) {
		rectangle.y += y;
	}
}